{{cookiecutter.repo_name}}VersionFileName = '../src/main/resources/{{cookiecutter.repo_name}}Version.properties'


def get_version():
    with open({{cookiecutter.repo_name}}VersionFileName) as {{cookiecutter.repo_name}}VersionFile:
        versionFileContent = {{cookiecutter.repo_name}}VersionFile.read()
    return versionFileContent[versionFileContent.rindex('version=') + 8:].strip()


def define_env(env):
    """
    This is the hook for defining variables, macros and filters

    - variables: the dictionary that contains the environment variables
    - macro: a decorator function, to declare a macro.
    """
    env.conf['site_name'] = '{{cookiecutter.project_name}} ' + get_version()

    @env.macro
    def {{cookiecutter.repo_name}}_version():
        return get_version()


if __name__ == '__main__':
    print(get_version())
