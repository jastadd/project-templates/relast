# {{cookiecutter.project_name}} Documentation

[{{cookiecutter.project_name}}]({{cookiecutter.repo_url}}) is a tool using [JastAdd](http://jastadd.org/).

## The Grammar

![](diagrams/{{cookiecutter.grammar_name}}Grammar.png)

