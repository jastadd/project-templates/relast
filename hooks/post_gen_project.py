import subprocess

PROJECT_URL = '{{cookiecutter.project_url}}'


def main():
    subprocess.call(['git', 'init'])
    subprocess.call(['git', 'remote', 'add', 'origin', PROJECT_URL])


if __name__ == '__main__':
    main()
