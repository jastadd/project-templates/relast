import re
import sys

MODULE_REGEX = r'^[_a-zA-Z][_a-zA-Z0-9]+$'

repo_name = '{{cookiecutter.repo_name}}'
grammar_name = '{{cookiecutter.grammar_name}}'


def main():
    if not re.match(MODULE_REGEX, repo_name):
        print('Error: repo_name "%s" is not a valid URL and Java package name' % repo_name)
        sys.exit(1)

    if not grammar_name.isalnum():
        print('Error: grammar_name "%s" must be a single word. Exiting.' % grammar_name)
        sys.exit(1)


if __name__ == '__main__':
    main()
