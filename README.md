# Cookiecutter template for relational RAG projects

Requirements:

- Cookiecutter (0.7.0+) installed, e.g., using `pip install cookiecutter`.

Usage:

1. Create a [new project](https://git-st.inf.tu-dresden.de/projects/new). Note the URL to clone the project.
2. Run `cookiecutter https://git-st.inf.tu-dresden.de/jastadd/project-templates/relast` and put in your information, especially the URL from step 1 as `project_url`.

## Input parameters

| Parameter Name | Default | Description | Used in/for |
|---|---|---|---|
| `repo_name` |  `"test_repo"` | Name used in the URL of the project | Initialize `project_url`, `repo_url`, Generated project directory, last part of the Java package, prefix of version file, name of the published artifact |
| `project_name` |  `"My New Relational RAG Project"` | (Long) name of the project | Title in README, pages |
| `grammar_name` |  `"MyNewRelationalRAGProject"` | Name of the grammar (initialized from project name) | Name of the grammar, partly name of the test |
| `namespace` |  `"jastadd"` | Namespace of the gitlab repository | Namespace used in the URL of the project |
| `project_url` |  `"git@git-st.inf.tu-dresden.de:{{cookiecutter.namespace}}/{{cookiecutter.repo_name}}.git"` | (pre-computed) URL to clone the project using SSH | Setting git `origin` remote |
| `repo_url` |  `"https://git-st.inf.tu-dresden.de/{{cookiecutter.namespace}}/{{cookiecutter.repo_name}}"` | (pre-computed) URL of the project | Back-link to repository in pages |
| `version` |  `"0.0.1"` | Initial version of the project | version file |
